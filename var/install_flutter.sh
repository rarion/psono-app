set -e

apt-get --quiet update --yes
apt-get --quiet install --yes wget tar xz-utils git
git config --global --add safe.directory '*'
wget --quiet --output-document=flutter-sdk.tar.xz $FLUTTER_VERSION
tar -xf flutter-sdk.tar.xz
export PATH=$PATH:$PWD/flutter/bin
echo flutter.sdk=$PWD/flutter > android/local.properties
flutter config --no-analytics
flutter --version
