#!/usr/bin/env bash
apt-get --quiet update --yes
apt-get --quiet install --yes curl

echo "Deploy Android to browserstack"
wget --quiet --output-document=psono-$CI_COMMIT_REF_NAME-debug.apk https://psono.jfrog.io/psono/psono/app/$CI_COMMIT_REF_NAME/app-debug.apk
curl -u "$BROWSERSTACK_USER:$BROWSERSTACK_PASSWORD" -X POST "https://api-cloud.browserstack.com/app-live/upload" -F "file=@`pwd`/psono-$CI_COMMIT_REF_NAME-debug.apk" -F "data={\"custom_id\": \"$CI_COMMIT_REF_NAME\"}"

echo "Deploy iOS to browserstack"
wget --quiet --output-document=psono-$CI_COMMIT_REF_NAME.ipa https://psono.jfrog.io/psono/psono/app/$CI_COMMIT_REF_NAME/app.ipa
curl -u "$BROWSERSTACK_USER:$BROWSERSTACK_PASSWORD" -X POST "https://api-cloud.browserstack.com/app-live/upload" -F "file=@`pwd`/psono-$CI_COMMIT_REF_NAME.ipa" -F "data={\"custom_id\": \"$CI_COMMIT_REF_NAME\"}"
