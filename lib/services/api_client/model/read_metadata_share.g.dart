// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_metadata_share.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadMetadataShare _$ReadMetadataShareFromJson(Map<String, dynamic> json) =>
    ReadMetadataShare(
      id: json['id'] as String?,
      writeDate: json['write_date'] as String?,
      shares: (json['shares'] as List<dynamic>?)
          ?.map((e) => ReadMetadataShare.fromJson(e as Map<String, dynamic>))
          .toList(),
      secrets: (json['secrets'] as List<dynamic>?)
          ?.map((e) => ReadMetadataSecret.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ReadMetadataShareToJson(ReadMetadataShare instance) =>
    <String, dynamic>{
      'id': instance.id,
      'write_date': instance.writeDate,
      'shares': instance.shares,
      'secrets': instance.secrets,
    };
