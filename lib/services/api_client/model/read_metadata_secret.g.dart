// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_metadata_secret.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadMetadataSecret _$ReadMetadataSecretFromJson(Map<String, dynamic> json) =>
    ReadMetadataSecret(
      writeDate: json['write_date'] as String?,
      id: json['id'] as String?,
    );

Map<String, dynamic> _$ReadMetadataSecretToJson(ReadMetadataSecret instance) =>
    <String, dynamic>{
      'write_date': instance.writeDate,
      'id': instance.id,
    };
