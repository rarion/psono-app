// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_metadata_datastore.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadMetadataDatastore _$ReadMetadataDatastoreFromJson(
        Map<String, dynamic> json) =>
    ReadMetadataDatastore(
      writeDate: json['write_date'] as String?,
      shares: (json['shares'] as List<dynamic>?)
          ?.map((e) => ReadMetadataShare.fromJson(e as Map<String, dynamic>))
          .toList(),
      secrets: (json['secrets'] as List<dynamic>?)
          ?.map((e) => ReadMetadataSecret.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ReadMetadataDatastoreToJson(
        ReadMetadataDatastore instance) =>
    <String, dynamic>{
      'write_date': instance.writeDate,
      'shares': instance.shares,
      'secrets': instance.secrets,
    };
