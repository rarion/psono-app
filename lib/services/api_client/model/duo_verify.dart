import 'package:json_annotation/json_annotation.dart';

part 'duo_verify.g.dart';

@JsonSerializable()
class DuoVerify {
  DuoVerify();

  factory DuoVerify.fromJson(Map<String, dynamic> json) =>
      _$DuoVerifyFromJson(json);

  Map<String, dynamic> toJson() => _$DuoVerifyToJson(this);
}
