import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/converter.dart' as converter;

class AccountOverviewScreen extends StatefulWidget {
  static String tag = 'account-overview-screen';

  @override
  _AccountOverviewScreenState createState() => _AccountOverviewScreenState();
}

class _AccountOverviewScreenState extends State<AccountOverviewScreen> {
  final userId = TextEditingController(
    text: reduxStore.state.userId,
  );
  final username = TextEditingController(
    text: reduxStore.state.username,
  );
  final email = TextEditingController(
    text: reduxStore.state.userEmail,
  );
  final publicKey = TextEditingController(
    text: converter.toHex(reduxStore.state.publicKey),
  );
  final serverApiVersion = TextEditingController(
    text: '',
  );
  final serverVersion = TextEditingController(
    text: '',
  );
  final serverSignature = TextEditingController(
    text: '',
  );
  final serverAuditLogging = TextEditingController(
    text: '',
  );
  final serverPublicKey = TextEditingController(
    text: '',
  );
  final serverLicenseType = TextEditingController(
    text: '',
  );
  final serverMaxUsers = TextEditingController(
    text: '',
  );
  final serverLicenseValidFrom = TextEditingController(
    text: '',
  );
  final serverLicenseValidTill = TextEditingController(
    text: '',
  );

  Future<void> loadServerInfo() async {
    apiClient.Info packageInfo = await apiClient.info();
    serverApiVersion.text = packageInfo.api.toString();
    serverVersion.text = packageInfo.version!;
    serverSignature.text = converter.toHex(packageInfo.signature)!;
    serverAuditLogging.text = packageInfo.logAudit.toString();
    serverPublicKey.text = converter.toHex(packageInfo.publicKey)!;

    if (packageInfo.licenseType == null) {
      serverLicenseType.text = 'Community Edition (CE)';
    } else if (packageInfo.licenseType == 'paid') {
      serverLicenseType.text = 'Enterprise Edition (EE)';
    } else {
      serverLicenseType.text = 'Enterprise Edition (EE) limited';
    }

    if (packageInfo.licenseMaxUsers == null) {
      serverMaxUsers.text = 'unlimited';
    } else {
      serverMaxUsers.text = packageInfo.licenseMaxUsers.toString();
    }

    if (packageInfo.licenseValidFrom == null) {
      serverLicenseValidFrom.text = 'N/A';
    } else {
      serverLicenseValidFrom.text = packageInfo.licenseValidFrom.toString();
    }

    if (packageInfo.licenseValidTill == null) {
      serverLicenseValidTill.text = 'N/A';
    } else {
      serverLicenseValidTill.text = packageInfo.licenseValidTill.toString();
    }
  }

  @override
  void initState() {
    super.initState();
    loadServerInfo();
  }

  @override
  void dispose() {
    userId.dispose();
    username.dispose();
    email.dispose();
    publicKey.dispose();
    serverApiVersion.dispose();
    serverVersion.dispose();
    serverSignature.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'OVERVIEW',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: const Color(0xFFebeeef),
      body: Card(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Text(
                  FlutterI18n.translate(context, "CLIENT_INFO"),
                ),
                const SizedBox(height: 8.0),
                TextField(
                  readOnly: true,
                  controller: userId,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'USER_ID',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: username,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'USERNAME',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: email,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'EMAIL',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: publicKey,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'PUBLIC_KEY',
                    ),
                  ),
                ),
                const SizedBox(height: 32.0),
                Text(
                  FlutterI18n.translate(context, "SERVER_INFO"),
                ),
                const SizedBox(height: 8.0),
                TextField(
                  readOnly: true,
                  controller: serverApiVersion,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_API_VERSION',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverVersion,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_VERSION',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverSignature,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_SIGNATURE',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverAuditLogging,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_AUDIT_LOGGING',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverPublicKey,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_PUBLIC_KEY',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverLicenseType,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_LICENSE_TYPE',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverMaxUsers,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_MAX_USERS',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverLicenseValidFrom,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_LICENSE_VALID_FROM',
                    ),
                  ),
                ),
                TextField(
                  readOnly: true,
                  controller: serverLicenseValidTill,
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'SERVER_LICENSE_VALID_TILL',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
