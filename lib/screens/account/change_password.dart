import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class AccountChangePasswordScreen extends StatefulWidget {
  static String tag = 'account-change-password-screen';

  @override
  _AccountChangePasswordScreenState createState() =>
      _AccountChangePasswordScreenState();
}

class _AccountChangePasswordScreenState
    extends State<AccountChangePasswordScreen> {
  final newPassword = TextEditingController(
    text: '',
  );
  final newPasswordRepeat = TextEditingController(
    text: '',
  );
  final oldPassword = TextEditingController(
    text: '',
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    newPassword.dispose();
    newPasswordRepeat.dispose();
    oldPassword.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    late apiClient.Info info;

    void _showErrorDiaglog(String title, String? content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(FlutterI18n.translate(context, title)),
            content: Text(FlutterI18n.translate(context, content!)),
            actions: <Widget>[
              TextButton(
                child: Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'CHANGE_PASSWORD',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: const Color(0xFFebeeef),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnSuccess(
            onPressed: () async {
              component.Loader.show(context);
              try {
                info = await apiClient.info();
              } on apiClient.ServiceUnavailableException {
                component.Loader.hide();
                _showErrorDiaglog(
                  FlutterI18n.translate(context, "SERVER_OFFLINE"),
                  FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                );
                return;
              } on HandshakeException {
                component.Loader.hide();
                _showErrorDiaglog(
                  FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
                  FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
                );
                return;
              } catch (e) {
                component.Loader.hide();
                _showErrorDiaglog(
                  FlutterI18n.translate(context, "SERVER_OFFLINE"),
                  FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                );
                return;
              }

              if (!_formKey.currentState!.validate()) {
                return;
              }
              try {
                await managerDatastoreUser.saveNewPassword(
                  newPassword.text,
                  newPasswordRepeat.text,
                  oldPassword.text,
                );
              } catch (e) {
                component.Loader.hide();
                _showErrorDiaglog('ERROR', e.toString());
                return;
              }

              newPassword.text = '';
              newPasswordRepeat.text = '';
              oldPassword.text = '';
              component.Loader.hide();

              if (!mounted) {
                return;
              }
              final SnackBar snackBar = SnackBar(
                content: Text(
                  FlutterI18n.translate(context, "SAVE_SUCCESS"),
                ),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            text: FlutterI18n.translate(context, "SAVE"),
          ),
        ),
      ),
      body: Card(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                component.AlertInfo(
                  text: FlutterI18n.translate(
                      context, "CHANGE_PASSWORD_DESCRIPTION"),
                ),
                TextFormField(
                  obscureText: true,
                  controller: newPassword,
                  autofocus: true,
                  validator: (value) {
                    String? testFailure = helper.isValidPassword(
                      value ?? '',
                      newPasswordRepeat.text,
                      info.complianceMinMasterPasswordLength,
                      info.complianceMinMasterPasswordComplexity,
                    );
                    if (testFailure == null) {
                      return null;
                    }
                    return FlutterI18n.translate(
                      context,
                      testFailure,
                    );
                  },
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'NEW_PASSWORD',
                    ),
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  controller: newPasswordRepeat,
                  autofocus: true,
                  validator: (value) {
                    if (value!.isNotEmpty && value != newPassword.text) {
                      return FlutterI18n.translate(
                        context,
                        'PASSWORDS_DONT_MATCH',
                      );
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'NEW_PASSWORD_REPEAT',
                    ),
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  controller: oldPassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'OLD_PASSWORD_REQUIRED',
                      );
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'OLD_PASSWORD',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
