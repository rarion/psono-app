import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class AccountDeleteAccountScreen extends StatefulWidget {
  static String tag = 'account-delete-account-screen';

  @override
  _AccountDeleteAccountScreenState createState() =>
      _AccountDeleteAccountScreenState();
}

class _AccountDeleteAccountScreenState
    extends State<AccountDeleteAccountScreen> {
  final password = TextEditingController(
    text: '',
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    component.Loader.hide();
    password.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    void _showErrorDiaglog(String title, String? content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(FlutterI18n.translate(context, title)),
            content: Text(FlutterI18n.translate(context, content!)),
            actions: <Widget>[
              TextButton(
                child: Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'DELETE_ACCOUNT',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: component.BtnDanger(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              component.Loader.show(context);
              try {
                await managerDatastoreUser.deleteAccount(password.text);
              } catch (e) {
                _showErrorDiaglog('ERROR', e.toString());
                return;
              } finally {
                component.Loader.hide();
              }
              if (mounted) {
                Navigator.pushReplacementNamed(context, '/signin/');
              }
            },
            text: FlutterI18n.translate(context, "DELETE"),
          ),
        ),
      ),
      backgroundColor: const Color(0xFFebeeef),
      body: Card(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                component.AlertDanger(
                  text: FlutterI18n.translate(
                      context, "YOU_ARE_ABOUT_TO_DELETE_YOUR_ACCOUNT"),
                ),
                TextFormField(
                  obscureText: true,
                  controller: password,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'OLD_PASSWORD_REQUIRED',
                      );
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'YOUR_PASSWORD_AS_CONFIRMATION',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
