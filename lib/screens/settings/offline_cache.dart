import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/offline_cache.dart' as offlineCache;
import 'package:sqlite3/sqlite3.dart';

class OfflineCacheDataSource extends DataTableSource {
  OfflineCacheDataSource(
    this.context,
    this.updateCacheSingle,
  );

  final BuildContext context;
  final void Function(String endpoint)? updateCacheSingle;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index > rowCount) return null;

    ResultSet? result = offlineCache.database?.select("""SELECT * FROM (
        SELECT
            ROW_NUMBER () OVER ( 
                ORDER BY endpoint
            ) row_num,
            write_date_milliseconds,
            endpoint
        FROM
            offline_cache
    ) t
    WHERE 
        row_num = ?""", [index + 1]);
    if (result == null) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Container(
          width: MediaQuery.of(context).size.width - 180,
          child: Text(result.first['endpoint']),
        )),
        DataCell(IconButton(
          icon: const Icon(Icons.refresh),
          onPressed: () {
            updateCacheSingle!(result.first['endpoint']);
          },
        )),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount {
    ResultSet? result =
        offlineCache.database?.select("""SELECT COUNT(*) as counter
      FROM offline_cache""");
    if (result == null) {
      return 0;
    }
    return result.first['counter'];
  }

  @override
  int get selectedRowCount => 0;
}

class SettingOfflineCacheScreen extends StatefulWidget {
  static String tag = 'settings-datastore-screen';

  @override
  _SettingOfflineCacheScreenState createState() =>
      _SettingOfflineCacheScreenState();
}

class _SettingOfflineCacheScreenState extends State<SettingOfflineCacheScreen> {
  bool initializing = offlineCache.database == null;
  bool? success;

  Future<void> updateCache() async {
    bool newSuccess = await offlineCache.incrementalUpdate(ratelimit: false);

    setState(() {
      success = newSuccess;
    });

    Timer(Duration(milliseconds: 5000), () {
      setState(() {
        success = null;
      });
    });
  }

  Future<void> updateCacheSingle(String endpoint) async {
    bool newSuccess = true;
    try {
      if (endpoint == '/datastore/') {
        await apiClient.readDatastoreList(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
        );
      } else if (endpoint == '/share/right/') {
        await apiClient.readShareRightsOverview(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
        );
      } else if (endpoint.startsWith('/share/')) {
        await apiClient.readShare(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          endpoint.substring('/share/'.length, endpoint.length - 1),
        );
      } else if (endpoint.startsWith('/datastore/')) {
        await apiClient.readDatastore(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          endpoint.substring('/datastore/'.length, endpoint.length - 1),
        );
      } else if (endpoint.startsWith('/secret/')) {
        await apiClient.readSecret(
          reduxStore.state.token,
          reduxStore.state.sessionSecretKey,
          endpoint.substring('/secret/'.length, endpoint.length - 1),
        );
      }
    } catch (e) {
      newSuccess = false;
    }

    setState(() {
      success = newSuccess;
    });

    Timer(Duration(milliseconds: 5000), () {
      setState(() {
        success = null;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      if (offlineCache.database == null) {
        setState(() {
          initializing = true;
        });

        await offlineCache.init();
        setState(() {
          initializing = offlineCache.database == null;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    if (success == true) {
      children.add(component.AlertInfo(
        text: FlutterI18n.translate(
          context,
          'SUCCESS_CACHE_UPDATED',
        ),
      ));
    } else if (success == false) {
      children.add(component.AlertWarning(
        text: FlutterI18n.translate(
          context,
          'ERROR_CACHE_NOT_UPDATED',
        ),
      ));
    }

    if (initializing == true) {
      children.add(component.AlertWarning(
        text: FlutterI18n.translate(
          context,
          'OFFLINE_CACHE_INITIALIZING_PLEASE_WAIT',
        ),
      ));
    } else {
      children.add(SingleChildScrollView(
        child: PaginatedDataTable(
          columns: [
            DataColumn(
                label: Text(FlutterI18n.translate(
              context,
              'ENDPOINT',
            ))),
            DataColumn(
                label: Text(FlutterI18n.translate(
              context,
              'REFRESH',
            ))),
          ],
          source: OfflineCacheDataSource(
            context,
            updateCacheSingle,
          ),
          rowsPerPage: 10,
        ),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'OFFLINE_CACHE',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () async {
              component.Loader.show(context);
              await updateCache();
              component.Loader.hide();
            },
          )
        ],
      ),
      backgroundColor: const Color(0xFFebeeef),
      body: ListView(
        shrinkWrap: true,
        children: children,
      ),
    );
  }
}
