import 'dart:async';

import 'package:flutter/material.dart';
import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:psono/services/offline_cache.dart' as offlineCache;
import 'package:sentry/sentry.dart';

Future<void> main() async {
  await Sentry.init(
    (options) {
      options.dsn =
          'https://7b17edbedc9244da8c16f3c30db4b0fe@sentry.io/1527081';
    },
    appRunner: initApp, // Init your App.
  );
}

Future<void> initApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  await offlineCache.init();
  Timer(Duration(milliseconds: 10000), () {
    offlineCache.incrementalUpdate(ratelimit: true);
  });
  var configuredApp = AppConfig(
    appName: 'Psono',
    flavorName: 'production',
    defaultServerUrl: 'https://www.psono.pw/server',
    child: MyApp(),
  );
  runApp(configuredApp);
}
