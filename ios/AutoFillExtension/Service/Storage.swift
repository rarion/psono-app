//
//  Storage.swift
//  AutoFillExtension
//

import Foundation

class StorageService {
    
    private var query:NSMutableDictionary!
    private var serverUrl:String!
    private var secretKey:String!
    private var sessionSecretKey:String!
    private var token:String!
    
    public required init() {
        self.query = NSMutableDictionary()
        self.query[kSecClass] = kSecClassGenericPassword
        self.query[kSecAttrService] = "flutter_secure_storage_service"
    }

    func read(key:String!, groupId:String! = nil) -> String! {
        let search:NSMutableDictionary! = self.query.mutableCopy() as? NSMutableDictionary
        if groupId != nil {
         search[kSecAttrAccessGroup] = groupId
        }
        search[kSecAttrAccount] = key
        search[kSecReturnData] = kCFBooleanTrue as Any

        var resultData: CFTypeRef?

        let status = SecItemCopyMatching((search as CFDictionary), &resultData)
        var value:String!
        if status == noErr {
            let data:NSData! = resultData as? NSData
            value = String(data:data as Data, encoding:String.Encoding.utf8)
        }

        return value
    }

    func readAll(groupId:String! = nil) -> NSDictionary! {
        let search:NSMutableDictionary! = self.query.mutableCopy() as? NSMutableDictionary
        if groupId != nil {
            search[kSecAttrAccessGroup] = groupId
        }

        search[kSecReturnData] = (kCFBooleanTrue as Any)

        search[kSecMatchLimit] = (kSecMatchLimitAll as Any)
        search[kSecReturnAttributes] = (kCFBooleanTrue as Any)

        var resultData: CFTypeRef?

        let status = SecItemCopyMatching((search as CFDictionary), &resultData)
        guard status != errSecItemNotFound else { return NSDictionary()}
        guard status == errSecSuccess else { return NSDictionary() }

        let items:[AnyObject]! = resultData as? [CFTypeRef]

        let results:NSMutableDictionary! = NSMutableDictionary()
        for case let item as NSDictionary in items {
            let key:String! = item[kSecAttrAccount] as? String
            let value:String! = String(data:item[kSecValueData] as! Data, encoding:String.Encoding.utf8)
            results[key!] = value
         }
        return results.copy() as? NSDictionary
    }
    
    func delete(key:String!, groupId:String! = nil) -> Void {
        let search:NSMutableDictionary! = self.query.mutableCopy() as? NSMutableDictionary
        if groupId != nil {
         search[kSecAttrAccessGroup] = groupId
        }
        search[kSecAttrAccount] = key

        SecItemDelete((search as CFDictionary))
    }
    
    func write(key:String!, value:String!, groupId:String! = nil) -> Void {
        let search:NSMutableDictionary! = self.query.mutableCopy() as? NSMutableDictionary
        if groupId != nil {
         search[kSecAttrAccessGroup] = groupId
        }
        search[kSecAttrAccount] = key
        search[kSecMatchLimit] = kSecMatchLimitOne as Any

        let status = SecItemCopyMatching((search as CFDictionary), nil)
        
        if status == noErr {
            // update
            search[kSecMatchLimit] = nil
            let update: NSMutableDictionary = NSMutableDictionary()
            update[kSecValueData] = value?.data(using: .utf8)
            update[kSecAttrAccessible] = kSecAttrAccessibleWhenUnlocked
            SecItemUpdate((search as CFDictionary), (update as CFDictionary))
            
        } else {
            // create
            search[kSecValueData] = value?.data(using: .utf8)
            search[kSecMatchLimit] = nil
            search[kSecAttrAccessible] = kSecAttrAccessibleWhenUnlocked
            SecItemAdd((search as CFDictionary), nil)
        }
        
    }
}

let storage = StorageService()
